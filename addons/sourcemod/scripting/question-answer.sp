#pragma semicolon 1
#include <csgo_colors>
#include <hlstatsX_adv>
#pragma newdecls required

Handle QAkv = INVALID_HANDLE;
//int		g_iRepeatQuestion = 1;				// сколько будет вопросов у всех
int		g_iRepeats[MAXPLAYERS+1];			// сколько осталось вопросов
int		g_iNowQuestion[MAXPLAYERS+1];		// какой вопрос сейчас у клиента
bool	g_bGoodQA[MAXPLAYERS+1];			// прошел ли игрок тест в этой сессии?
bool	g_bTesmMe[MAXPLAYERS+1];			// игрок проходит тест по собственному желанию

public Plugin myinfo =
{
	name = "[JAIL] Question-Answer",
	author = "BetmanSmall && ShaRen",
	description = "Question-Answer for Jail-Mod server",
	version = "1.0.4.0",
	url = "Servers-Info.Ru"
};

public void OnPluginStart()
{
	RegConsoleCmd("jointeam", cmdJoinTeam);
	RegConsoleCmd("sm_testme", cmdTestMe);
}

public Action cmdTestMe(int client, int args)
{
	if (IsClientInGame(client)) {
		g_iRepeats[client] = 40;
		g_bTesmMe[client] = true;
		CreateMenuQuestionAnswer(client);
	}
	return Plugin_Continue;
}

public Action cmdJoinTeam(int client, int args)
{
	if (!IsClientInGame(client))
		return Plugin_Continue;
	// Get args.
	char text[192];
	if (!GetCmdArgString(text, sizeof(text))) {
		return Plugin_Handled;
	}
	int startidx = 0;
	if(text[strlen(text)-1] == '"') {
		text[strlen(text)-1] = '\0';
		startidx = 1;
	}
	int newTeam = StringToInt(text[startidx]);
	int oldTeam = GetClientTeam(client);
	if (!oldTeam)	// если 1й раз за сессию выбираешь команду
		g_bGoodQA[client] = false;
	if(!newTeam) {	// если автовыбор
		ChangeClientTeam(client, 2);
		return Plugin_Handled;
	}
	else if ( newTeam == 3) {
		if(!g_bGoodQA[client]) {
			g_iRepeats[client] = GetQuestionsForPlayer(client);
			g_bTesmMe[client] = false;
			CreateMenuQuestionAnswer(client);
			if (!oldTeam)
				ChangeClientTeam(client, 1);
			return Plugin_Handled;
		}
		ChangeClientTeam(client, 3);
		return Plugin_Handled;
	}
	else {
		ChangeClientTeam(client, newTeam);
		return Plugin_Handled;
	}
}

int GetQuestionsForPlayer(int client)
{
	int time = GetClientPlayedTime(client);
	if (time == -1)		// если не определен
		return 2;
	if (time < 1800)	// 30 мин
		return 32;
	if (time < 3600)	// 1ч
		return 16;
	if (time < 7200)	// 2ч
		return 8;
	if (time < 14400)	// 4ч
		return 4;
	if (time < 21600)	// 6ч
		return 2;
	if (time < 28800)	// 8ч
		return 1;
	else
		return 0;
}

void ShowAbout(int client, int question)
{
	if (QAkv != INVALID_HANDLE)
		CloseHandle(QAkv);
	QAkv = CreateKeyValues("Question-Answer");
	char sPath[256];
	BuildPath(Path_SM, sPath, sizeof(sPath), "configs/question-answer.ini");
	if (FileExists(sPath)) {
		if (!FileToKeyValues(QAkv, sPath))
			SetFailState("Can not convert file to KeyValues: %s", sPath);
		KvRewind(QAkv);
		char sQuestion[10];
		IntToString(question, sQuestion, sizeof(sQuestion));
		if(KvJumpToKey(QAkv, sQuestion)) {
			if(KvJumpToKey(QAkv, "about")) {
				char sNumAbout[10];
				char sAbout[1024];
				int i=1;
				while(i < 15) {
					IntToString(i, sNumAbout, sizeof(sNumAbout));
					KvGetString(QAkv, sNumAbout, sAbout, sizeof(sAbout), "none");
					if (StrEqual(sAbout, "none"))
						break;
					CGOPrintToChat(client, "%s", sAbout);
					i++;
				}
			}
			else LogError("Not found case: about");
		}
		else LogError("Not found case: %d", 1);
	}
	else SetFailState("File Not Found: %s", sPath);
}

public Action CreateMenuQuestionAnswer(int client)
{
	if (QAkv != INVALID_HANDLE)
		CloseHandle(QAkv);
	QAkv = CreateKeyValues("Question-Answer");
	char sPath[256];
	BuildPath(Path_SM, sPath, sizeof(sPath), "configs/question-answer.ini");
	if (FileExists(sPath)) {
		if (!FileToKeyValues(QAkv, sPath))
			SetFailState("Can not convert file to KeyValues: %s", sPath);
		KvRewind(QAkv);
		int MaxQuestion = 0;
		for (bool result = KvGotoFirstSubKey(QAkv); result; result = KvGotoNextKey(QAkv))
			MaxQuestion ++;
		if ( !MaxQuestion )
			SetFailState("Can not find any data in file: %s", sPath);
		KvRewind(QAkv);
		Handle menu = CreateMenu(ChoiceMenu);
		int time;
		int attempt = MaxQuestion;
		while(attempt > 0) {
			int iRandomCase = GetRandomInt(1,MaxQuestion);
			g_iNowQuestion[client] = iRandomCase;
			char sRandomCase[256];
			IntToString(iRandomCase, sRandomCase, sizeof(sRandomCase));
			if(KvJumpToKey(QAkv, sRandomCase)) {
				char sQuestion[256];
				KvGetString(QAkv, "question", sQuestion, sizeof(sQuestion), "none");
				if(!StrEqual(sQuestion, "none")) {
					SetMenuTitle(menu, sQuestion);
					int iCount = KvGetNum(QAkv, "count", 0);
					if(iCount) {
						int iNumTrueAnswer = KvGetNum(QAkv, "true", 0);
						if(iNumTrueAnswer && iNumTrueAnswer<=iCount) {
							char sAnswer[256];
							char sTrueAnswer[256];
							char sNumTrueAnswer[256];
							IntToString(iNumTrueAnswer, sNumTrueAnswer, sizeof(sNumTrueAnswer));
							KvGetString(QAkv, sNumTrueAnswer, sTrueAnswer, sizeof(sTrueAnswer), "none");
							if(!StrEqual(sTrueAnswer, "none")) {
								int iRanTrueSelectItem = GetRandomInt(1,iCount);	// какой № будет верный 
								char sTmpAnswer[256];
								char sRanTrueSelectItem[256];
								IntToString(iRanTrueSelectItem, sRanTrueSelectItem, sizeof(sRanTrueSelectItem));
								KvGetString(QAkv, sRanTrueSelectItem, sTmpAnswer, sizeof(sTmpAnswer), "none");	// что было под новым правильным пунктом
								if(!StrEqual(sTmpAnswer, "none"))
									for(int num = 1; num<=iCount; num++) {
										if(num == iRanTrueSelectItem)		// если этот номер правильный 
											AddMenuItem(menu, "true", sTrueAnswer);
										else if(num == iNumTrueAnswer)		// если под этим номер был правльный ответ
											AddMenuItem(menu, "false", sTmpAnswer);
										else {	// sTrueAnswer и sTmpAnswer поменяли местами, остальное все по порядку как в конфиге
											char snum[256];
											IntToString(num, snum, sizeof(snum));
											KvGetString(QAkv, snum, sAnswer, sizeof(sAnswer), "none");
											if(!StrEqual(sAnswer, "none"))
												AddMenuItem(menu, "false", sAnswer);
											else {
												LogError("File structure is interrupted: Bad section '%d', case: %d", num, iRandomCase);
												attempt--;
												KvGoBack(QAkv);
												break;
											}
										}
									}
								else {
									LogError("File structure is interrupted: Bad section '%d', case: %d", iRanTrueSelectItem, iRandomCase);
									attempt--;
									KvGoBack(QAkv);
								}
							}
							else {
								LogError("File structure is interrupted: Bad section '%d', case: %d", iNumTrueAnswer, iRandomCase);
								attempt--;
								KvGoBack(QAkv);
							}
							time = KvGetNum(QAkv, "time", 20);
							attempt = -1;
						}
						else {
							LogError("File structure is interrupted: Bad section 'true', case: %d", iRandomCase);
							attempt--;
							KvGoBack(QAkv);
						}
					}
					else {
						LogError("File structure is interrupted: Bad section 'count', case: %d", iRandomCase);
						attempt--;
						KvGoBack(QAkv);
					}
				}
				else {
					LogError("File structure is interrupted: Bad section 'question', case: %d", iRandomCase);
					attempt--;
					KvGoBack(QAkv);
				}
			}
			else {
				LogError("Not found case: %d", iRandomCase);
				attempt--;
			}
		}
		if(!attempt)
			SetFailState("File structure is interrupted: Bad ALL section: rewrite File!");
		else if(attempt == -1) {
			SetMenuExitButton(menu, true);
			if(g_bTesmMe[client])
				DisplayMenu(menu, client, 100);
			else DisplayMenu(menu, client, time);
		}
	}
	else SetFailState("File Not Found: %s", sPath);
}

public int ChoiceMenu(Handle menu, MenuAction action, int client, int choice)
{
	if(client > 0 && IsClientInGame(client)) {
		if (action == MenuAction_Select) {
			char info[32];
			GetMenuItem(menu, choice, info, sizeof(info));
			if(StrEqual(info, "true")) {
				if(g_iRepeats[client]) {
					g_iRepeats[client]--;
					CGOPrintToChat(client, "{LIGHTBLUE}Верно, едем дальше!");
					CreateMenuQuestionAnswer(client);
				}
				else {
					CGOPrintToChat(client, "{BLUE}Все верно!");
					g_bGoodQA[client] = true;
					if (!g_bTesmMe[client])
						ChangeClientTeam(client, 3);
				}
			}
			else {
				CGOPrintToChat(client, "{RED}Ответ не правильный!");
				ShowAbout(client, g_iNowQuestion[client]);
				CGOPrintToChat(client, "Ссылка на правила: {BLUE}http://goo.gl/RnJ9P0 {DEFAULT} Пиши {BLUE}!testme{DEFAULT} чтобы тренероваться");
				CGOPrintToChat(client, "Нашли ошибку, хотите добавить вопрос? skype: {BLUE}sharteman{DEFAULT}");
				if (g_bTesmMe[client])
					CreateMenuQuestionAnswer(client);
			}
		}
		else if (action == MenuAction_Cancel)
			CGOPrintToChat(client, "{RED}Вы не ответили");
	}
	else if (action == MenuAction_End)
		CloseHandle(menu);
}